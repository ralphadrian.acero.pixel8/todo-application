<?php
    // api class
    class API {
        // declare Properties
        private $age;
        private $email;
        private $birthday;

        // class methods
        function echo_name($name) {             // echoes name from input
            echo "Full Name: {$name} <br>";
        }

        function echo_hobbies($hobbies) {       // echoes hobbies
            echo "Hobbies: <br>";
            foreach($hobbies as $hobby) {
                echo "\t{$hobby}<br>";
            }
        }
                                   
        function echo_age_email_bday(){         // echoes age, email, and birthday
            echo "Age: {$this->age} <br>";
            echo "Email: {$this->email} <br>";
            echo "Birthday: {$this->birthday} <br>";
        }

        // setters for age, email, and birthday
        function set_age($age) {
            $this->age = $age;
        }

        function set_email($email){
            $this->email = $email;
        }

        function set_bday($birthday){
            $this->birthday = $birthday;
        }
    }
    // create API class instance
    $new_api = new API();
    
    // set object values
    $new_api->set_age(21);
    $new_api->set_email("ralphadrian.acero.pixel8@gmail.com");
    $new_api->set_bday("November 21, 2001");
    
    // call methods to display output
    $new_api->echo_name("Ralph Adrian Acero");
    $new_api->echo_hobbies(array("Reading Books", "Swimming", "Watch Youtube" ));
    $new_api->echo_age_email_bday();
?>