<!-- 
    Project name: PHP MYSQL Simple Crud
    Author: Ralph Adrian Jacob Acero
    Date: July 10, 2023
    Description: Performs simple crud operations on the given employee table using form POSTS
 -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>CREATE</h3>
    <form action="task7.php" method="POST">
        <label for="first_name">Enter your first name:</label>
        <input type="text" id="first_name" name="first_name"> <br>
        <label for="middle_name">Enter your middle name:</label> 
        <input type="text" id="middle_name" name="middle_name"> <br>
        <label for="last_name">Enter your last name:</label>
        <input type="text" id="last_name" name="last_name"> <br>
        <label for="birthday">Birthday:</label>
        <input type="text" id="birthday" name="birthday" placeholder="1990-01-01"> <br>
        <label for="address">Address:</label>
        <input type="text" id="address" name="address"> <br>
        
        <button type="submit" name="create">Create</button>
    </form>

    <h3>READ</h3>
    <form action="task7.php" method="POST">
        <select name="field">
            <option value="">Select column attribute</option>
            <option value="id">id</option>
            <option value="first_name">first name</option>
            <option value="middle_name">middle name</option>
            <option value="last_name">last name</option>
            <option value="birthday">birthday</option>
            <option value="address">address</option>
        </select> <br>
        <label for="attribute">Enter attribute value:</label>
        <input type="text" id="attribute" name="attribute"> <br>
        <button type="submit" name="search">Search</button>
    </form>

    <h3>UPDATE</h3>
    <form action="task7.php" method="POST">
        <select name="update_field">
            <option value="">Select column to be modified</option>
            <option value="id">id</option>
            <option value="first_name">first name</option>
            <option value="middle_name">middle name</option>
            <option value="last_name">last name</option>
            <option value="birthday">birthday</option>
            <option value="address">address</option>
        </select> <br>
        <label for="update_attribute">Enter new value:</label>
        <input type="text" id="update_attribute" name="update_attribute"> <br>
        <select name="criteria_field">    
            <option value="">Select column criteria</option>
            <option value="id">id</option>
            <option value="first_name">first name</option>
            <option value="middle_name">middle name</option>
            <option value="last_name">last name</option>
            <option value="birthday">birthday</option>
            <option value="address">address</option>
        </select> <br>
        <label for="criteria_attribute">Enter value:</label>
        <input type="text" id="criteria_attribute" name="criteria_attribute"> <br>
        <button type="submit" name="update">Update</button>
    </form>

    <h3>DELETE</h3>
    <form action="task7.php" method="POST">
        <select name="delete_field">
            <option value="">Select column attribute</option>
            <option value="id">id</option>
            <option value="first_name">first name</option>
            <option value="middle_name">middle name</option>
            <option value="last_name">last name</option>
            <option value="birthday">birthday</option>
            <option value="address">address</option>
        </select> <br>
        <label for="delete_attribute">Enter attribute value:</label>
        <input type="text" id="delete_attribute" name="delete_attribute"> <br>
        <button type="submit" name="delete">Delete</button>
    </form>
    
    <h3>RESULTS</h3>
</body>
</html>
<?php 
// Connect to the database using MySQLi
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'task6';

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Verify connection
if ($conn) {
    // echo "Successfully connected to the database! <br>";
} else {
    echo "Error: Could not connect to the database <br>";
}

// Insert data into the employee table
if (isset($_POST['create'])) {
    $notempty = true;
    if(empty($_POST['first_name'])){
        $notempty = false;
    }elseif(empty($_POST['middle_name'])){
        $notempty = false;
    }elseif(empty($_POST['last_name'])){
        $notempty = false;
    }elseif(empty($_POST['birthday'])){
        $notempty = false;
    }elseif(empty($_POST['address'])){
        $notempty = false;
    }
    $firstName = $_POST['first_name'];
    $middleName = $_POST['middle_name'];
    $lastName = $_POST['last_name'];
    $birthday = $_POST['birthday'];
    $address = $_POST['address'];

    if($notempty){
        $sql = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address) VALUES (?, ?, ?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sssss", $firstName, $lastName, $middleName, $birthday, $address);    
        
        
        if ($stmt->execute()) {
            echo "New record created successfully <br>";
        } else {
            echo "Error: " . $sql . "<br>" . $stmt->error;
        }

        $stmt->close();
    }else {
        echo "Some fields were left empty!!";
    }

}

// Search data in the employee table
if (isset($_POST['search'])) {
    $column = $_POST['field'];
    $value = $_POST['attribute'];

    $sql = "SELECT * FROM employee WHERE {$column}=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $value);

    if ($stmt->execute()) {
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "First Name: " . $row["first_name"] . " - Last Name: " . $row["last_name"] . " - Birthday: " . $row["birthday"] . "<br>";
            }
        } else {
            echo "No results found <br>";
        }
    } else {
        echo "Error: " . $sql . "<br>" . $stmt->error;
    }

    $stmt->close();
}

// Update data in the employee table
if (isset($_POST['update'])) {
    $update_column = $_POST['update_field'];
    $update_value = $_POST['update_attribute'];
    $criteria_column = $_POST['criteria_field'];
    $criteria_value = $_POST['criteria_attribute'];

    $sql = "UPDATE employee SET {$update_column}=? WHERE {$criteria_column}=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ss", $update_value, $criteria_value);

    if ($stmt->execute()) {
        $affectedRows = $stmt->affected_rows;
        if ($affectedRows > 0) {
            echo "Record updated successfully <br>";
        } else {
            echo "No records updated <br>";
        }
    } else {
        echo "Error updating record: " . $stmt->error;
    }

    $stmt->close();
}


// Search data in the employee table
if (isset($_POST['delete'])) {
    $column = $_POST['delete_field'];
    $value = $_POST['delete_attribute'];

    $sql = "DELETE FROM employee WHERE {$column}=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $value);

    if ($stmt->execute()) {
        $affectedRows = $stmt->affected_rows;
        if ($affectedRows > 0) {
            echo "Record deleted successfully <br>";
        } else {
            echo "No records updated <br>";
        }
    } else {
        echo "Error updating record: " . $stmt->error;
    }

    $stmt->close();
}

$conn->close();
?>