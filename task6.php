<?php


// Connect to the database using MySQLi
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'task6';    // create a new database in phpmyadmin and named it task6
                        // imported the downloaded sql file

$conn = new mysqli($host, $username, $password, $database);


if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// verify connection
if($conn) {
    echo"Successfully connected to the database! <br>";
}else {
    echo"Error: Could not connect to the database <br>";
}


//START of QUERIES

// Insert data into the employee table
$sql = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address) VALUES ('Jane', 'Doe', 'Marie', '1990-01-01', '456 Elm Street')";// QUERY HERE !!!!!!;
if ($conn->query($sql) === TRUE) {
    echo "New record created successfully <br>";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

// retrieve the first name, last name, and birthday of all employees in the table
$sql = "SELECT first_name, last_name, birthday FROM employee"; 
$result = $conn->query($sql);

if($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"] . " - Birthday: " . $row["birthday"]. "<br>";
    }
}

// retrieve the number of employees whose last name starts with the letter 'D'
$sql = "SELECT COUNT(*) FROM employee WHERE last_name LIKE 'D%'"; // QUERY HERE !!!!!!;
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    echo "number of employees whose last name starts with 'D': {$result->num_rows} <br>"; 
} else {
    echo "0 results <br>";
}

//  retrieve the first name, last name, and address of the employee with the highest ID number
$sql = "SELECT first_name, last_name, address FROM employee WHERE id=(SELECT MAX(id) FROM employee)"; // QUERY HERE !!!!!!;
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Address: " . $row["address"]. "<br>";
    }
} else {
    echo "0 results";
}


// Update data in the employee table
$sql = "UPDATE employee SET address='123 Main Street' WHERE first_name='John'"; // QUERY HERE !!!!!!;
if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully <br>";
} else {
    echo "Error updating record: " . $conn->error;
}


// Delete data from the employee table
$sql = "DELETE FROM employee WHERE last_name LIKE 'D%'";// QUERY HERE !!!!!!;
if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully <br>";
} else {
    echo "Error deleting record: " . $conn->error;
}
$conn->close();


?>
