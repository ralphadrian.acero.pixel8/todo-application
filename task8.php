<!-- 
    Project name: PHP MYSQL Simple Crud
    Author: Ralph Adrian Jacob Acero
    Date: July 11, 2023
    Description: Convert the previous exercise to use thingengineer/mysqli-database-class
 -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>CREATE</h3>
    <form action="task8.php" method="POST">
        <label for="first_name">Enter your first name:</label>
        <input type="text" id="first_name" name="first_name"> <br>
        <label for="middle_name">Enter your middle name:</label> 
        <input type="text" id="middle_name" name="middle_name"> <br>
        <label for="last_name">Enter your last name:</label>
        <input type="text" id="last_name" name="last_name"> <br>
        <label for="birthday">Birthday:</label>
        <input type="text" id="birthday" name="birthday" placeholder="1990-01-01"> <br>
        <label for="address">Address:</label>
        <input type="text" id="address" name="address"> <br>
        
        <button type="submit" name="create">Create</button>
    </form>

    <h3>READ</h3>
    <form action="task8.php" method="POST">
        <select name="field">
            <option value="">Select column attribute</option>
            <option value="id">id</option>
            <option value="first_name">first name</option>
            <option value="middle_name">middle name</option>
            <option value="last_name">last name</option>
            <option value="birthday">birthday</option>
            <option value="address">address</option>
        </select> <br>
        <label for="attribute">Enter attribute value:</label>
        <input type="text" id="attribute" name="attribute"> <br>
        <button type="submit" name="search">Search</button>
    </form>

    <h3>UPDATE</h3>
    <form action="task8.php" method="POST">
        <select name="update_field">
            <option value="">Select column to be modified</option>
            <option value="id">id</option>
            <option value="first_name">first name</option>
            <option value="middle_name">middle name</option>
            <option value="last_name">last name</option>
            <option value="birthday">birthday</option>
            <option value="address">address</option>
        </select> <br>
        <label for="update_attribute">Enter new value:</label>
        <input type="text" id="update_attribute" name="update_attribute"> <br>
        <select name="criteria_field">    
            <option value="">Select column criteria</option>
            <option value="id">id</option>
            <option value="first_name">first name</option>
            <option value="middle_name">middle name</option>
            <option value="last_name">last name</option>
            <option value="birthday">birthday</option>
            <option value="address">address</option>
        </select> <br>
        <label for="criteria_attribute">Enter value:</label>
        <input type="text" id="criteria_attribute" name="criteria_attribute"> <br>
        <button type="submit" name="update">Update</button>
    </form>

    <h3>DELETE</h3>
    <form action="task8.php" method="POST">
        <select name="delete_field">
            <option value="">Select column attribute</option>
            <option value="id">id</option>
            <option value="first_name">first name</option>
            <option value="middle_name">middle name</option>
            <option value="last_name">last name</option>
            <option value="birthday">birthday</option>
            <option value="address">address</option>
        </select> <br>
        <label for="delete_attribute">Enter attribute value:</label>
        <input type="text" id="delete_attribute" name="delete_attribute"> <br>
        <button type="submit" name="delete">Delete</button>
    </form>
    
    <h3>RESULTS</h3>
</body>
</html>
<?php 
require_once __DIR__ . '/vendor/thingengineer/mysqli-database-class/MysqliDb.php';
// Connect to the database using MySQLi
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'task6';

$conn = new mysqli($host, $username, $password, $database);
$db = new MysqliDb($conn);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Verify connection
if ($conn) {
    // echo "Successfully connected to the database! <br>";
} else {
    echo "Error: Could not connect to the database <br>";
}

// Insert data into the employee table
if (isset($_POST['create'])) {
    $notempty = true;
    if(empty($_POST['first_name'])){
        $notempty = false;
    }elseif(empty($_POST['middle_name'])){
        $notempty = false;
    }elseif(empty($_POST['last_name'])){
        $notempty = false;
    }elseif(empty($_POST['birthday'])){
        $notempty = false;
    }elseif(empty($_POST['address'])){
        $notempty = false;
    }

    $firstName = $_POST['first_name'];
    $middleName = $_POST['middle_name'];
    $lastName = $_POST['last_name'];
    $birthday = $_POST['birthday'];
    $address = $_POST['address'];

    $data = array("first_name" => $firstName, "last_name" => $lastName, "middle_name" => $middleName, "birthday" => $birthday, "address"=>$address);
    

    if($notempty){
        $id = $db->insert('employee', $data);
        if ($id) {
            echo "New record created successfully <br>";
        } else {
            echo "Error: " . $db->getLastError() . "<br>";
        }
    }else {
        echo "Some fields were left empty!!";
    }

}

// Search data in the employee table - uses a simple SELECT - WHERE query and displays the details
if (isset($_POST['search'])) {
    $column = $_POST['field'];
    $value = $_POST['attribute'];

    $db->where($column, $value);
    $employees = $db->get("employee");
    if($db->count > 0) {
        foreach($employees as $employee) {
            echo "First Name: " . $employee["first_name"] . " - Middle Name: " . $employee["middle_name"] . " - Last Name: " . $employee["last_name"] . " - Birthday: " . $employee["birthday"] . "<br>";
        }
    }
}

// Update data in the employee table - gets the column to be updated and the new value and the criteria column and value of the row to be updated
if (isset($_POST['update'])) {
    $update_column = $_POST['update_field'];
    $update_value = $_POST['update_attribute'];
    $criteria_column = $_POST['criteria_field'];
    $criteria_value = $_POST['criteria_attribute'];

    $data = array($update_column => $update_value);
    $db->where($criteria_column, $criteria_value);
    if($db->update('employee', $data)){
        echo $db->count . " Record updated successfully <br>";
    } else {
        echo "No records updated <br>" . $db->getLastError();
    }
}


// Delete data in the employee table - uses a column and a value as input to delete a row
if (isset($_POST['delete'])) {
    $column = $_POST['delete_field'];
    $value = $_POST['delete_attribute'];

    $db->where($column, $value);
    if($db->delete('employee') && $db->count > 0){
        echo "Record deleted successfully <br>";   
    }
}

$conn->close();
?>